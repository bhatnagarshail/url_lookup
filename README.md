
SERVER
Usage: server <Server IP> <Request-Port> <Update-Port>

The first argument is the IP address to which the server should be bound.

Server is monitoring the <Update-Port> for databse updates. The message 
contains the URL that is not allowed to be accessed by clients. On receiving
this URL, the server queries its database and takes the following action:
a) If URL is already present in its database, it responds with a message
"DUPLICATE"
b) If URL is not present in its database, it response with a message
"ADDED"
c) If it encounters an internal error accessing its database, it responds with
"INTERNAL_ERROR"
So the server responds with a simple text back to the application that sends
database updates.

Server is monitoring <Request-Port> for policing client URL requests. Upto 255 
client requests can be handled simultaneously. This is via a #define and can
be increased by modifying it and recompiling the program. On receiving the 
request URL, it queries its database and takes following action:
a) IF URL is found in the database, the client is sent a message containing
"NACK"  - indicating that request is illegal.
b) IF URL is NOT found in the database, the client is sent a message 
containing "ACK" - meaning it can proceed with the request.

Client requests are assumed to have a form like this (starting with "GET")
GET /urlinfo/1/nicesite.com/path?q=something+allowed
http: is not currently supported in the URL string. URL comparison is via
simple string comparison, so if following 2 URLS are equivalent, they would
not be considered equal :

GET /urlinfo/1/abcd.com/homepage
GET /urlinfo/1/abcd.com

"GET" itself is not stored in the database and any trailing white space is 
deleted before saving.

The hash function to convert a URL into an integer is taken from the KErnighan
& Ritchie "C" programming book. The URL is stored in a file name which is 
obtained by concatenating the string "url_database_" with the integer 
obtained by hashing the URL string. So if a given URL hash to integer 3289, it
will be stored in a file name "url_database_3289".
Upto 4000 URL database files will be created while handling database updates
because a prime number 4001 is used in the hash function for obtaining the
remainder. So URL database files will be like 
url_database_0, url_database_1 .... url_database_4000
Depending on where the input URL gets hashed to.

So this helps reduce the lookup time because in the worst case, only the 
contents of one file need to be compared . (unless most URLs are hashing to
the same file which will not generally be the case)

CLIENT
Usage: client <HTTP Proxy Server IP> <Port> <URL File>

The first argument is the IP address of the Proxy server that handles both
database updates and client URL policing.The second argument is the destination
port number for the request. This client can be used both as a regular HTTP
client and as a database update vehicle, depending upon the destination
port chosen in the command line. 
THe URL file contains the List of URLS that the client is trying to get validated
or it is trying to update the proxy server with rogue URLs i.e. URLS that 
should be stored in the server database.

Program to generate test/Rogue URLs:
A test program has been written to generate a file containing bad URLs. This
file can be generated via the test program and then used as an input file for
the client program. The program simply takes the number of bad URLs to be
generated and they can be redirected to an output file. This output file can
then be used as input to the client program for sending database update to
the server.



Design Questions:
1) The size of the URL list could grow infinitely, how might you scale this 
beyond the memory capacity of this VM? 

Response:
In the implementation provided, the URL database is stored on disk in 
multiple files, so the memory/RAM capacity of the VM is not an issue.


2) The number of requests may exceed the capacity of this VM, how might you 
solve that? 

Response:
We could use a load balancer application, running on a machine that is 
always supposed to remain up. The load balancer acts as a front line for
handling client requests and for performing database updates. There can be
a certain number of worker nodes/servers that perform the actual operation.
For handling requests, the load balancer can chose a backend node and the 
backend server can respond directly to the client about the URL. This keeps
the load distributed across nodes. 

3) What are some strategies you might use to update the service with new URLs? 
Updates may be as much as 5 thousand URLs a day with updates arriving every 
10 minutes.

Response:
For handling database updates, the load balancer can still forward the request 
to a backend node. The only thing is database updates will require file based 
locking for exclusive access to the file. Database/file lookups can be done 
safely without locking. OF course this would be limited by the amount of 
disk space that is available to the application/VM.

4) Bonus points if you containerize the app

Kubernetes provides the necessary infrastructure for solving problem like this
including the scaling aspect of this problem.

There is another possibility here. The proxy server address would have to be
resolved via a DNS lookup. The DNS server could have the intelligence to return
a different IP address e.g. if there are 4 backend servers, each server needs 
access to its own copy of the URL database. The DNS server will return an IP 
address in round robin fashion or some algorithm built into the server. So the 
requests will get farmed out to different servers, thereby causing load 
distribution. In this case, the database needs to be replicated periodically
across the 4 nodes. So whenever a node receives a new URL in its database,
it would have to replicate the URL to the other server nodes.
