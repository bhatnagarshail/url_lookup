#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

static int req_fd;
static unsigned short server_port;

int
setup_client_socket (const char *server_ip, unsigned short port_num)
{
    int fd;
    struct sockaddr_in server_addr;

    fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
        perror("socket");
        return -1;
    }


    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(server_ip);
    server_addr.sin_port = htons(port_num);

    if (connect(fd, (const struct sockaddr *)&server_addr, 
                sizeof(server_addr)) == -1) {
        perror("connect");
        return -1;
    }

    return fd;
}

static char url_str[4096];
int
main(int argc, char *argv[])
{
    int status;
    pid_t pid;
    FILE *fp;
    char status_buf[64];
    int nbytes;
    int retval;

    if (argc != 4) {
        fprintf(stderr, "Usage: %s <HTTP Proxy Server IP> <Port> <URL File>\n",
             argv[0]);
        return -1;
    }

    server_port = atoi(argv[2]);
    req_fd = setup_client_socket(argv[1], server_port);
    if (req_fd == -1) {
        return (-1);
    }
    
    fp = fopen(argv[3], "r");
    if (fp == NULL) {
        perror("Cannot open input file");
        close(req_fd);
        return -1;
    }

    while (fgets(url_str, sizeof(url_str) - 1, fp)) {
        nbytes = strlen(url_str);
        url_str[nbytes - 1] = 0;
        pid = fork();
        if (pid == 0) {
            if (send(req_fd, url_str, nbytes + 1, 0) == -1) {
                perror("send");
            } else {
                if (recv(req_fd, status_buf, sizeof(status_buf) - 1, 0) == -1) {
		    perror("recv");
		} else {
		    printf("Req [%s] Response [%s]\n", url_str, status_buf);
		}
            }
            break;
        } else {
            waitpid(pid, &status, 0);
        }
    }

    fclose(fp);
    close(req_fd);
    return (0);
}
