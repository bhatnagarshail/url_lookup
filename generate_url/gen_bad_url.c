#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    unsigned int idx, num_bad_urls;

    if (argc != 2) {
        fprintf(stderr, 
            "Usage: %s <Number of Bad URL to generate>\n", argv[0]);
        return -1;
    }

    num_bad_urls = atoi(argv[1]);
    for (idx = 0; idx < num_bad_urls; ++idx) {
        printf("/urlinfo/1/badsite%d.com/path?q=something+bad%d\n", idx, idx);
    }
    return (0);
}

