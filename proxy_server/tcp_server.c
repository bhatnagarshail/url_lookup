#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <signal.h>


#define MAX_ACTIVE_CLIENT (256)
#define HASHSIZE 4001     /* Prime number */
#define SKIP_WHITESPACE(str) while (*str && (*str == ' ' || *str == '\t')) \
                                 str++;

/*
 * Table containing connection information about active HTTP clients
 * or clients that update the URL databse
 */
typedef struct {
    int fd;
    struct sockaddr_in client_addr;
    unsigned char update_conn;
} connection_table_t;

static connection_table_t fd_table[MAX_ACTIVE_CLIENT];
static unsigned int active_client_count;

/*
 * fd and port corresponding to http requests to be policed by this server
 */
static int req_fd;
static unsigned short req_port;

/*
 * fd and port corresponding to URL updates received by this server
 */
static int update_fd;
static unsigned short update_port;

unsigned int
k_n_r_hash (char *str)
{
    unsigned long hashval;

    /*
     * This simple hash function is taken from Kernighan & Ritchie "C" book
     * MAp a URL string to an integer
     */
    for (hashval = 0; *str != '\0'; str++) {
        hashval = *str + (31 * hashval);
    }

    return hashval % HASHSIZE;
}

static int
add_client_entry (struct sockaddr_in *client_addr, int new_fd, int conn_fd)
{
    /*
     * Store the client information in the global connection table
     */
    if (active_client_count == MAX_ACTIVE_CLIENT) {
        fprintf(stderr, "Client connection table full\n");
        return -1;
    }
    fd_table[active_client_count].client_addr = *client_addr;
    fd_table[active_client_count].update_conn = (conn_fd == update_fd);
    fd_table[active_client_count++].fd = new_fd;
    return 0;
}

static int
delete_client_entry (int client_fd)
{
    /*
     * Delete client information in the global connection table
     */
    unsigned int idx;

    for (idx = 0; idx < active_client_count; ++idx) {
        if (fd_table[idx].fd == client_fd) {
            memmove(fd_table + idx, fd_table + idx + 1, 
                    (active_client_count - idx - 1));
            active_client_count--;
            return (0);
        }
    }
    return (-1);
}

int
setup_server_socket (const char *ip_addr, unsigned short port_num,
                     unsigned int max_pending_client)
{
    int fd;
    struct sockaddr_in my_addr;

    fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (fd == -1) {
        perror("socket");
        return -1;
    }


    memset(&my_addr, 0, sizeof(my_addr));
    my_addr.sin_family = AF_INET;
    my_addr.sin_addr.s_addr = inet_addr(ip_addr);
    my_addr.sin_port = htons(port_num);
    if (bind(fd, (const struct sockaddr *)&my_addr, sizeof(my_addr)) == -1) {
        perror("bind");
        return -1;
    }

    if (listen(fd, max_pending_client) == -1) {
        perror("listen");
        return -1;
    }
    return fd;
}

int
url_compare (const char *url1, const char *url2)
{
    // fprintf(stderr, "Comparing [%s] and [%s]\n", url1, url2);
    return strcmp(url1, url2);
}

static const char *
update_database (char *read_buf, unsigned int nbytes)
{
    FILE *fp;
    int url_len;
    char url_buf[1024];
    char fname[1024];
    unsigned int idx = k_n_r_hash(read_buf);

    snprintf(fname, sizeof(fname), "url_database_%d", idx);
    fprintf(stderr, "URL [%s] mapped to file [%s]\n", read_buf, fname);
    fp = fopen(fname, "a+");
    if (fp == NULL) {
        perror("fopen url database");
        return "INTERNAL_ERROR";
    }

    rewind(fp);
    while (fgets(url_buf, sizeof(url_buf) - 1, fp)) {
        url_len = strlen(url_buf);
        if (url_buf[url_len - 1] == '\n') {
            url_buf[url_len - 1] = '\0';
        }
        if (url_compare(url_buf, read_buf) == 0) {
            // fprintf(stderr, "URL %s already exists in database\n", read_buf);
            fclose(fp);
            return "DUPLICATE";
        }
    }
    fprintf(fp, "%s\n", read_buf);
    fprintf(stderr, "URL %s added to database file %s\n", read_buf, fname);
    fclose(fp);
    return "ADDED";
}

static int
lookup_database (char *read_buf)
{
    FILE *fp;
    int nbytes;
    char url_buf[1024];
    char fname[1024];
    unsigned int idx = k_n_r_hash(read_buf);

    snprintf(fname, sizeof(fname), "url_database_%d", idx);
    fprintf(stderr, "URL [%s] mapped to file [%s]\n", read_buf, fname);
    fp = fopen(fname, "r");
    if (fp == NULL) {
        return -1;
    }

    while (fgets(url_buf, sizeof(url_buf), fp)) {
        nbytes = strlen(url_buf);
        if (url_buf[nbytes - 1] == '\n') {
            url_buf[nbytes - 1] = '\0';
        }
        if (url_compare(url_buf, read_buf) == 0) {
            // fprintf(stderr, "URL %s already exists in database\n", read_buf);
            fclose(fp);
            return 0;
        }
    }
    fclose(fp);
    return (1);
}

void
server_main_loop (void)
{
    int retval, nbytes;
    int new_fd;
    int curr_fd;
    char read_buf[1024];
    fd_set read_set;
    fd_set read_set_copy;
    socklen_t addr_len;
    struct sockaddr_in client_addr;
    const char *ret_str;
    unsigned int idx;
    char *url_ptr;
    int max_fd = (req_fd > update_fd) ? (req_fd + 1) : (update_fd + 1);

    FD_ZERO(&read_set);
    FD_SET(req_fd, &read_set);
    FD_SET(update_fd, &read_set);
    read_set_copy = read_set;

    while (1) {
        retval = select(max_fd, &read_set, NULL, NULL, NULL);
        if (retval > 0) {
            for (curr_fd = req_fd, idx = 0; idx < 2; ++idx) {
                if (FD_ISSET(curr_fd, &read_set)) {
                    fprintf(stderr, "%s readable\n", (curr_fd == req_fd) ?
                        "Request FD" : "Update FD");
                    addr_len = sizeof(client_addr);
                    new_fd = accept(curr_fd, (struct sockaddr *)&client_addr,
                                    &addr_len);
                    if (new_fd == -1) {
                        perror("accept");
                    } else {
                        add_client_entry(&client_addr, new_fd, curr_fd);
                        FD_SET(new_fd, &read_set_copy);
                        if (max_fd <= new_fd) {
                            max_fd = new_fd + 1;
                        }
                        fprintf(stderr, "Added new fd %d max_fd %d\n",
                                new_fd, max_fd);
                    }
                }
                curr_fd = update_fd;
            }

            for (idx = 0; idx < active_client_count; ++idx) {
                if (FD_ISSET(fd_table[idx].fd, &read_set)) {
                    nbytes = recv(fd_table[idx].fd, read_buf, 
                                  sizeof(read_buf) - 1, 0);
                    if (nbytes <= 0) {
                        close(fd_table[idx].fd);
                        delete_client_entry(fd_table[idx].fd);
                        FD_CLR(fd_table[idx].fd, &read_set_copy);
                        if ((fd_table[idx].fd + 1) == max_fd) {
                            max_fd--;
                        }
                        fprintf(stderr, "Removed client fd %d"
                            " new max_fd %d\n", fd_table[idx].fd, max_fd);
                    } else {
                        url_ptr = read_buf + nbytes - 1;
                        while (url_ptr >= read_buf) {
                            if (url_ptr[0] == '\0' || url_ptr[0] == '\n' ||
                                url_ptr[0] == ' '  || url_ptr[0] == '\t') {
                                *url_ptr-- = '\0';
                            } else {
                                break;
                            }
                        }
                        if (url_ptr <= read_buf) {
                            continue;
                        }

                        url_ptr = read_buf;
                        SKIP_WHITESPACE(url_ptr);
                        if (strncmp(url_ptr, "GET", 3) == 0) {
                            url_ptr += 3;
                            SKIP_WHITESPACE(url_ptr);
                        }

                        if (fd_table[idx].update_conn) {
                            ret_str = update_database(url_ptr, nbytes);
                            send(fd_table[idx].fd, ret_str, strlen(ret_str), 0);
                        } else {
                            if (lookup_database(url_ptr) == 0) {
                                send(fd_table[idx].fd, "NACK", 4, 0);
                            } else {
                                send(fd_table[idx].fd, "ACK", 3, 0);
                            }
                        }
                    }
                }
            }
        } else if (retval == 0) {
            perror("select timeout");
        } else {
            perror("select");
        }
        read_set = read_set_copy;
    }
}

void *
sigint_handler (int param1)
{
    unsigned int idx;

    close(req_fd);
    close(update_fd);
    for (idx = 0; idx < active_client_count; ++idx) {
        close(fd_table[idx].fd);
    }
    exit(-1);
}

int
main(int argc, char *argv[])
{
    if (argc != 4 && argc != 5) {
        fprintf(stderr, "Usage: %s <Server IP> <Request-Port> <Update-Port>\n",
             argv[0]);
        return -1;
    }

    sigset(SIGINT, sigint_handler);

    req_port = atoi(argv[2]);
    update_port = atoi(argv[3]);
    req_fd = setup_server_socket(argv[1], req_port, 
                                             MAX_ACTIVE_CLIENT);
    if (req_fd == -1) {
        return (-1);
    }
    update_fd = setup_server_socket(argv[1], update_port, 1);
    if (update_fd == -1) {
        close(req_fd);
        return (-1);
    }

    fprintf(stderr, "Listening for Client requests on port %d\n"
            "Listening for Updates on port %d\n", req_port, update_port);

    server_main_loop();
    return (0);
}
